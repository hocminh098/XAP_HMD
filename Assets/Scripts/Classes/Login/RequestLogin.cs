using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class RequestLogin
{
    public string email;
    public string password;
}

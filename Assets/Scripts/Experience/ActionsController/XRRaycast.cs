// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;

// public class XRRaycast : MonoBehaviour
// {
//     #region Singleton pattern 
//     private static XRRaycast instance;
//     public static XRRaycast Instance
//     {
//         get
//         {
//             if (instance == null)
//             {
//                 instance = FindObjectOfType<XRRaycast>();
//             }
//             return instance;
//         }
//     }
//     #endregion singketon pattern 

//     public RaycastObject GetObjectRaycast(GameObject controller)
//     {
//         XRRayInteractor xrInteractor = controller.GetComponent<XRRayInteractor>();
//         if (xrInteractor == null)
//         {
//             return null;
//         }

//         if (xrInteractor.TryGetCurrent3DRaycastHit(out RaycastHit raycastHit))
//         {
//             if (raycastHit.collider.tag == TagConfig.ORGAN 
//             && raycastHit.collider.transform.parent = VRObjectManager.Instance.CurrentObject.transform)
//             {
//                 return new RaycastObject(raycastHit.collider.gameObject, raycastHit.point);
//             }
//         }
//         return null;
//     }

//     public bool IsHoverObject(GameObject controller)
//     {
//         XRRayInteractor xrInteractor = controller.GetComponent<XRRayInteractor>();
//         if (xrInteractor == null)
//         {
//             return false;
//         }
//         if (xrInteractor.TryGetCurrent3DRaycastHit(out RaycastHit raycastHit))
//         {
//             if (raycastHit.collider.tag == TagConfig.ORGAN)
//             {
//                 return true;
//             }
//         }
//         return false;
//     }
// }

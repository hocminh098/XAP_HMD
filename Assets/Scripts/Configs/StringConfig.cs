using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StringConfig
{
    public static string audioPreText = "Audio ";
    public static string videoPreText = "Video ";
    public static string welcome = "Welcome back, ";
    public static string allLesson = "All lessons";
    public static string offset = "&offset=";
    public static string levelParentObject = "0";
    public static string levelParentObjectAppend = "0-";
    public static string letterAppend = "-";

    public static string DOT_SHORT_STRING = "...";

    // error
    public static string errorMessageServer = "Cannot connect to server. Please try again!";
    public static string errorInternetConnection = "Error. Check internet connection!";
}

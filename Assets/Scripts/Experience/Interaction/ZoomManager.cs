using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomManager : MonoBehaviour
{
   #region Singleton pattern
        private static ZoomManager instance;
        public static ZoomManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<ZoomManager>();
                }
                return instance;
            }
        }
    #endregion Singleton pattern
    public GameObject go_LeftHandController;
    public GameObject go_RightHandController;
    float originDelta;
    Vector3 originScale;
    float currentDelta;
    float scaleFactor;
    bool isMoved = false;

    public void Zoom()
    {
        if(TriggerManager.Instance.IsPressTriggerSameTimeOfController())
        {
            if (!isMoved)
            {
                originDelta = Vector2.Distance(go_LeftHandController.transform.position, go_RightHandController.transform.position);
                originScale = ARObjectManager.Instance.OriginObject.transform.localScale;
                isMoved = true;
            }
            else
            {
                currentDelta = Vector2.Distance(go_LeftHandController.transform.position, go_RightHandController.transform.position);
                scaleFactor = currentDelta / originDelta;
                ARObjectManager.Instance.OriginObject.transform.localScale = originScale * scaleFactor;
            }
        }
        else
        {
            isMoved = false;
        } 
    }
}

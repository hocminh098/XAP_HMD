using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class LoadSceneAsync
{
    public static IEnumerator LoadYourAsyncScene(string nameScene)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(nameScene);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
}
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// using TalesFromTheRift;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Login
{
    public class LoginManager : MonoBehaviour
    {
        #region UI Component
        public GameObject go_inputFieldEmail;
        private InputField inputFieldEmail;
        public GameObject go_inputFieldPassword;
        private InputField inputFieldPassword;
        public Button btnSignIn;
        public Button btnCancel;
        public GameObject btnVisibility;
        public GameObject btnClearAllValueEmail;
        public GameObject btnClearAllValuePassword;
        public GameObject txtNotifyValidateEmail;
        public GameObject txtNotifyValidatePassword;
        public GameObject txtNotification;
        public GameObject loading;

        #endregion UI Component

        #region Common Variables
        bool isClickBtnVisibility = false;
        bool isDataValid = false;

        #endregion Common Variables

        #region MonoBehaviour Method
        void Start()
        {
            InitUI();
            SetActions();
        }

        void Update()
        {
            OnFocusInputField(go_inputFieldEmail, inputFieldEmail);
            OnFocusInputField(go_inputFieldPassword, inputFieldPassword);
        }

        void OnEnable()
        {
            LoadData.onUserLogin += OnUserLogin;
            LoadData.onLoginFailed += OnLoginFailed;
        }


        void OnDisable()
        {
            LoadData.onUserLogin -= OnUserLogin;
            LoadData.onLoginFailed -= OnLoginFailed;
        }
        #endregion MonoBehaviour Method

        #region Private method

        void OnFocusInputField(GameObject go_inputField, InputField inputField)
        {
            if (inputField.isFocused)
            {
                // OpenCanvasKeyboard.Instance.OpenKeyboard(go_inputField);
            }
        }
        void SetActions()
        {
            btnSignIn.onClick.AddListener(LoginUser);
            btnCancel.onClick.AddListener(CancelPopup);
            btnVisibility.GetComponent<Button>().onClick.AddListener(DisplayPassword);
            btnClearAllValueEmail.GetComponent<Button>().onClick.AddListener(delegate { ClearAllValue(inputFieldEmail); });
            btnClearAllValuePassword.GetComponent<Button>().onClick.AddListener(delegate { ClearAllValue(inputFieldPassword); });
        }

        void InitUI()
        {
            inputFieldEmail = go_inputFieldEmail.GetComponent<InputField>();
            inputFieldEmail.contentType = InputField.ContentType.Standard;
            inputFieldPassword = go_inputFieldPassword.GetComponent<InputField>();
            inputFieldPassword.contentType = InputField.ContentType.Password;
        }

        void LoginUser()
        {
            btnSignIn.interactable = false;
            StartCoroutine(CheckValueOfInputFieldAndCallAPI(inputFieldEmail, inputFieldPassword));
        }

        void CancelPopup()
        {
            ClearAllValue(inputFieldEmail);
            ClearAllValue(inputFieldPassword);
        }

        IEnumerator CheckValueOfInputFieldAndCallAPI(InputField inputFieldEmail, InputField inputFieldPassword)
        {
            if (!ValidateDataUser.Instance.IsValidEmail(inputFieldEmail.text))
            {
                isDataValid = false;
                txtNotifyValidateEmail.SetActive(true);
                yield return new WaitForSeconds(3);
                btnSignIn.interactable = true;
                inputFieldEmail.Select();
                txtNotifyValidateEmail.SetActive(false);
            }
            else
                isDataValid = true;

            if (!ValidateDataUser.Instance.IsValidPassword(inputFieldPassword.text))
            {
                isDataValid = false;
                txtNotifyValidatePassword.SetActive(true);
                yield return new WaitForSeconds(3);
                btnSignIn.interactable = true;
                inputFieldPassword.Select();
                txtNotifyValidatePassword.SetActive(false);
            }
            else
                isDataValid = true;

            if (isDataValid)
            {
                LoadData.Instance.LoginUser(inputFieldEmail.text, inputFieldPassword.text);
                loading.SetActive(true);
            }
        }

        void DisplayPassword()
        {
            isClickBtnVisibility = !isClickBtnVisibility;
            if (isClickBtnVisibility)
            {
                inputFieldPassword.contentType = InputField.ContentType.Standard;
                inputFieldPassword.ForceLabelUpdate();
                btnVisibility.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>(PathResourceConfig.UNVISIBILITY_ICON);
            }
            else
            {
                inputFieldPassword.contentType = InputField.ContentType.Password;
                inputFieldPassword.ForceLabelUpdate();
                btnVisibility.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>(PathResourceConfig.VISIBILITY_ICON);
            }
        }

        void ClearAllValue(InputField inputField)
        {
            inputField.text = "";
        }

        void OnUserLogin()
        {
            loading.SetActive(false);
            if (PlayerPrefs.HasKey("token") && PlayerPrefs.GetString("token") != "")
            {
                StartCoroutine(LoadSceneAsync.LoadYourAsyncScene(SceneConfig.home));
            }
        }

        void OnLoginFailed(string message)
        {
            loading.SetActive(false);

            StartCoroutine(ShowNotification(message));
            btnSignIn.interactable = true;
        }

        IEnumerator ShowNotification(string message)
        {
            txtNotification.SetActive(true);
            txtNotification.GetComponent<Text>().text = message;
            yield return new WaitForSeconds(3);
            txtNotification.SetActive(false);
        }

        #endregion Private method
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Organ
{
    public string organsId;
    public string organsName;
    public string isActive;
    public string organsThumbnailId;
}

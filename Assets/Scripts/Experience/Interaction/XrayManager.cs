using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;
using UnityEngine.UI;

public class XrayManager : MonoBehaviour
{
    private static XrayManager instance;
    public static XrayManager Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<XrayManager>();
            return instance;
        }
    }

    private bool isMakingXRay;
    public bool IsMakingXRay
    {
        get
        {
            return isMakingXRay;
        }
        set
        {
            isMakingXRay = value;
            btnXray.GetComponent<Image>().sprite = isMakingXRay ? Resources.Load<Sprite>(PathResourceConfig.XRAY_CLICKED_IMAGE) : Resources.Load<Sprite>(PathResourceConfig.XRAY_UNCLICKED_IMAGE);
        }
    }

    public Dictionary<string, Material[]> DictionaryMaterialOriginal { get; set; } = new Dictionary<string, Material[]>();

    Material[] temp;
    int childCount;

    // UI
    public Button btnXray;
    public Material transparentDarkMaterial;
    Material[] newMaterials;

    void Start()
    {
        newMaterials = new Material[] { transparentDarkMaterial };
    }

    public void HandleXrayView(bool currentXrayStatus)
    {
        IsMakingXRay = currentXrayStatus;
        if (IsMakingXRay)
        {
            btnXray.interactable = false;
            ChangeMaterial(ARObjectManager.Instance.OriginObject);
            btnXray.interactable = true;
        }
        else
        {
            btnXray.interactable = false;
            BackToOriginMaterial(ARObjectManager.Instance.OriginObject);
            btnXray.interactable = true;
        }
    }

    public void GetOriginalMaterial(GameObject objectInstance)
    {
        var renderers = objectInstance.GetComponentsInChildren<MeshRenderer>(true);
        if (renderers.Length <= 0)
            return;

        foreach (var item in renderers)
            DictionaryMaterialOriginal.Add(item.gameObject.name, item.materials);
    }

    public void ChangeMaterial(GameObject objectInstance)
    {
        var renderers = objectInstance.GetComponentsInChildren<MeshRenderer>(true);

        if (renderers.Length <= 0)
            return;

        foreach (var item in renderers)
        {
            item.materials = newMaterials;
        }
    }

    public void BackToOriginMaterial(GameObject objectInstance)
    {
        var renderers = objectInstance.GetComponentsInChildren<MeshRenderer>(true);
        if (renderers.Length <= 0)
            return;

        foreach (var item in renderers)
        {
            if (DictionaryMaterialOriginal.TryGetValue(item.gameObject.name, out temp))
                item.materials = temp;
        }
    }

}

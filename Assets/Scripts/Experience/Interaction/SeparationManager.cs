using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SeparationManager : MonoBehaviour
{
    private static SeparationManager instance;
    public static SeparationManager Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<SeparationManager>();
            return instance;
        }
    }

    // constantly 
    private const float RADIUS = 8f;
    private const float DISTANCE_FACTOR = 2f;

    // variable
    private int childCount;
    private Vector3 centerPosCurrentObject;
    private Vector3 centerPosChildObject;
    private Vector3 targetPosition;
    private float angle;
    private bool isSeparating;
    public Button btnSeparate;
    public bool IsSeparating
    {
        get
        {
            return isSeparating;
        }
        set
        {
            isSeparating = value;
            btnSeparate.GetComponent<Image>().sprite = isSeparating ? Resources.Load<Sprite>(PathResourceConfig.SEPARATE_CLICKED_IMAGE) : Resources.Load<Sprite>(PathResourceConfig.SEPARATE_UNCLICKED_IMAGE);

        }
    }

    public void HandleSeparate(bool isSeparating)
    {
        IsSeparating = isSeparating;
        if (IsSeparating)
        {
            btnSeparate.interactable = false;
            SeparateOrganModel();
            btnSeparate.interactable = true;
        }
        else
        {
            btnSeparate.interactable = false;
            BackToPositionOrgan();
            btnSeparate.interactable = true;
        }
    }

    public void SeparateOrganModel()
    {
        childCount = ARObjectManager.Instance.CurrentObject.transform.childCount;
        centerPosCurrentObject = Helper.CalculateBounds(ARObjectManager.Instance.CurrentObject).center;

        foreach (Transform childTransform in ARObjectManager.Instance.CurrentObject.transform)
        {
            centerPosChildObject = Helper.CalculateBounds(childTransform.gameObject).center;
            targetPosition = ComputeTargetPosition(centerPosCurrentObject, centerPosChildObject);
            StartCoroutine(MoveObjectWithLocalPosition(childTransform.gameObject, targetPosition));
        }
    }

    public Vector3 ComputeTargetPosition(Vector3 center, Vector3 currentPosition)
    {
        Vector3 dir = currentPosition - center;
        return dir.normalized / ARObjectManager.Instance.FactorScaleInitial;
    }

    public IEnumerator MoveObjectWithLocalPosition(GameObject moveObject, Vector3 targetPosition)
    {
        float timeSinceStarted = 0f;
        while (true)
        {
            timeSinceStarted += Time.deltaTime;
            moveObject.transform.localPosition = Vector3.Lerp(moveObject.transform.localPosition, targetPosition, timeSinceStarted);

            if (moveObject.transform.localPosition == targetPosition)
                yield break;

            yield return null;
        }
    }
    public void BackToPositionOrgan()
    {
        if (ARObjectManager.Instance.ListOfInitialPositionOfChildren.Count < 1)
            return;

        int childCount = ARObjectManager.Instance.CurrentObject.transform.childCount;

        if (childCount < 0)
            return;

        for (int i = 0; i < childCount; i++)
        {
            targetPosition = ARObjectManager.Instance.ListOfInitialPositionOfChildren[i];
            StartCoroutine(MoveObjectWithLocalPosition(ARObjectManager.Instance.CurrentObject.transform.GetChild(i).gameObject, targetPosition));
        }
    }


}

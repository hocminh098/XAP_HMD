using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InteractionUILesson : MonoBehaviour
{
    #region UI Component
    public Text txtMainNavigation;
    public Button btnLogout;
    public Button btnLessons;
    public Button btnJoinMeeting;
    #endregion
    
    void Start()
    {
        SetActions();
    }

    void SetActions()
    {
        btnLessons.onClick.AddListener(HandlerLessons);
        btnJoinMeeting.onClick.AddListener(HandlerJoinMeeting);
    }
    void HandlerLessons()
    {
        SceneManager.LoadScene(SceneConfig.lesson);
    }
    void HandlerJoinMeeting()
    {
        SceneManager.LoadScene(SceneConfig.meetingCube);
    }
    void Update()
    {

    }
}

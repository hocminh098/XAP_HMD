using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingEffectManager : MonoBehaviour
{
    private static LoadingEffectManager instance;
    public static LoadingEffectManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindbjectOfType<LoadingEffectManager>();
            }
            return instance;
        }
    }

    public GameObject objectLoadingEffect;

    public void ShowLoadingEffect(bool isActiveEffect)
    {
        objectLoadingEffect.SetActive(isActiveEffect);

    }
    
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PathResourceConfig
{
    public const string PATH_UI_ITEM_ORGAN = "UI/Home/ItemOrgan";
    public const string PATH_UI_ITEM_GROUP_LESSON = "UI/Home/ItemGroupLesson";
    public const string PATH_UI_ITEM_LESSON = "UI/Home/ItemLesson";

    public const string VISIBILITY_ICON = "Icon/Common/Visibility";
    public const string UNVISIBILITY_ICON = "Icon/Common/UnVisibility";

    public const string AUDIO_ICON = "Icon/Common/Audio";
    public const string VIDEO_ICON = "Icon/Common/Video";

    public const string VIDEO_PAUSE_ICON = "Icon/Common/Video_Pause";
    public const string VIDEO_PLAY_ICON = "Icon/Common/Video_Play";

    public const string AUDIO_PAUSE_ICON = "Icon/Common/Pause_audio";
    public const string AUDIO_PLAY_ICON = "Icon/Common/Play_audio";

    public const string BG_BUTTON_RED = "Icon/Background/Button_red";
    public const string BG_BUTTON_WHITE = "Icon/Background/Button_white";


    // Icon folder
    public const string SEPARATE_UNCLICKED_IMAGE = "Icon/UnClicked/Separate";
    public const string SEPARATE_CLICKED_IMAGE = "Icon/Clicked/Separate";
    public const string XRAY_CLICKED_IMAGE = "Icon/Clicked/Xray";
    public const string XRAY_UNCLICKED_IMAGE = "Icon/UnClicked/Xray";
    public const string HOLD_CLICKED_IMAGE = "Icon/Clicked/Hold";
    public const string HOLD_UNCLICKED_IMAGE = "Icon/UnClicked/Hold";
    public const string LABEL_CLICKED_IMAGE = "Icon/Clicked/Label";
    public const string LABEL_UNCLICKED_IMAGE = "Icon/UnClicked/Label";


    // UI Experience folder
    public const string PATH_UI_NODE_TREE_BODY = "UI/Experience/ItemNodeTreeBody";
    public const string PATH_UI_NODE_TREE_HEADER = "UI/Experience/ItemNodeTreeHeader";
    // public const string PATH_UI_ITEM_ORGAN = "UI/Home/ItemOrgan";
    // public const string PATH_UI_ITEM_GROUP_LESSON = "UI/Home/ItemGroupLesson";
    // public const string PATH_UI_ITEM_LESSON = "UI/Home/ItemLesson";
    public const string MODEL_TAG_LABEL = "UI/Experience/Tag";
    public const string PATH_UI_ITEM_MEDIA = "UI/Experience/ItemMedia";

}

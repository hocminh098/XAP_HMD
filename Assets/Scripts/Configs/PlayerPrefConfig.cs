using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerPrefConfig
{
    public static string avatar = "avatar";
    public static string fullName = "fullName";
    public static string email = "email";
    public static string token = "token";
    public static string lessonId = "lessonId";
    public static string orderIdOrgan = "orderIdOrgan";
}

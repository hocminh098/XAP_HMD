using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
// using Photon.Voice.PUN;
// using TalesFromTheRift;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Lessons
{
    public class LessonManager : MonoBehaviour
    {
        private static LessonManager instance;
        public static LessonManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<LessonManager>();
                }
                return instance;
            }
        }

        #region UI component
        const int NUMER_LESSON_IN_ROW = 5; 
        const int NUMER_ROW = 2;
        public Text txtMainNavigation;
        public Button btnLogout;
        public Button btnJoinMeetingShort;
        public Button btnLessonShort;
        public GameObject lessonPanel;

        public Transform contentOrgan;
        public Transform contentLesson;
        // public Button btnExit;
        public GameObject loadingHome;
        public GameObject listOrgans;
        public GameObject listLessons;
        public GameObject btnLoadMore;
        public int currentPage = 0;
        public CompositeLesson CompositeLesson { get; set; }
        public List<Lesson> Lessons { get; set; }
        public Organ[] Organs { get; set; }
        public int CurrentTotalPage { get; set; }
        #endregion UI component

        #region MonoBehaviour Method
        void Start()
        {
            CheckAuthenticationUser();
            SetActions();
        }

        #endregion MonoBehaviour Method

        void CheckAuthenticationUser()
        {
            if (PlayerPrefs.HasKey("token") && PlayerPrefs.GetString("token") != "")
                txtMainNavigation.text = StringConfig.welcome + PlayerPrefs.GetString("fullName");
            else
                StartCoroutine(LoadSceneAsync.LoadYourAsyncScene(SceneConfig.login));
        }

        void SetActions()
        {
            // navigate controll
            btnLogout.onClick.AddListener(SignOut);

            // lesson
            // btnLessons.onClick.AddListener(ShowLessons);
            btnLessonShort.onClick.AddListener(ShowLessons);

            // panigation
            btnLoadMore.GetComponent<Button>().onClick.AddListener(delegate { LoadLessonsWithPageNumber(); });
        }

        void SignOut()
        {
            PlayerPrefs.DeleteAll();
            currentPage = 0;
            StartCoroutine(LoadSceneAsync.LoadYourAsyncScene(SceneConfig.login));
        }

        public void ShowLessons()
        {
            currentPage = 0;
            PlayerPrefs.SetString(PlayerPrefConfig.orderIdOrgan, "");
            PlayerPrefs.Save();

            lessonPanel.SetActive(true);
            // mainControlPanel.SetActive(false);
            // joinMeetingPanel.SetActive(false);

            LoadDataOrgans();
            LoadLessonsWithPageNumber();
        }

        #region Action to do when invoke event
        void DisplayListOrgan()
        {
            loadingHome.SetActive(false);
            listOrgans.SetActive(true);

            // Item button all lesson
            GameObject itemOrganAll = CreateUIItem(PathResourceConfig.PATH_UI_ITEM_ORGAN, contentOrgan);
            itemOrganAll.transform.GetChild(0).GetComponent<Text>().text = StringConfig.allLesson;
            itemOrganAll.transform.GetComponent<Button>().onClick.AddListener(delegate { OnClickedItemOgran(); });
            itemOrganAll.transform.GetComponent<Button>().Select();

            // list item organ
            if (Organs != null)
            {
                if (Organs.Length > 0)
                {
                    foreach (Organ itemOrganData in Organs)
                    {
                        GameObject itemOrgan = CreateUIItem(PathResourceConfig.PATH_UI_ITEM_ORGAN, contentOrgan);
                        itemOrgan.transform.GetChild(0).GetComponent<Text>().text = itemOrganData.organsName;
                        itemOrgan.name = itemOrganData.organsId;
                        itemOrgan.transform.GetComponent<Button>().onClick.AddListener(delegate { OnClickedItemOgran(itemOrgan.name); });
                    }
                }
            }
            else
            {
                return;
            }
        }

        void DisplayListLesson()
        {
            currentPage++;
            // if (currentPage >= CurrentTotalPage)
            // btnLoadMore.SetActive(false);
            if (Lessons != null)
            {
                if (Lessons.Count > 0)
                {
                    if (loadingHome != null)
                        loadingHome.SetActive(false);

                    listLessons.SetActive(true);
                    for (int i = 1; i <= NUMER_ROW; i++)
                    {
                        GameObject itemGroupLesson = CreateUIItem(PathResourceConfig.PATH_UI_ITEM_GROUP_LESSON, contentLesson);
                        for (int j = NUMER_LESSON_IN_ROW * (i - 1); j < NUMER_LESSON_IN_ROW * i; j++)
                        {
                            if (j >= Lessons.Count)
                                return;
                            GameObject itemLesson = CreateUIItem(PathResourceConfig.PATH_UI_ITEM_LESSON, itemGroupLesson.transform);
                            itemLesson.transform.GetChild(1).GetComponent<Text>().text = Lessons[j].lessonTitle;

                            string imageURL = ApiConfig.DOMAIN_SERVER + Lessons[j].lessonThumbnail;
                            Image targetImage = itemLesson.transform.GetChild(0).GetComponent<Image>();
                            Davinci.get().load(imageURL).setFadeTime(0).setCached(true).into(targetImage).start();
                            itemLesson.transform.GetChild(2).gameObject.SetActive(false);
                            itemLesson.name = Lessons[j].lessonId;
                            itemLesson.transform.GetComponent<Button>().onClick.AddListener(delegate { OnClickedItemLesson(itemLesson.name); });
                        }
                    }
                }
            }
            else
            {
                return;
            }
        }
        #endregion Action to do when invoke event

        #region call API 
        async Task LoadLessonsWithPageNumber()
        {
            if (PlayerPrefs.HasKey(PlayerPrefConfig.orderIdOrgan) && PlayerPrefs.GetString(PlayerPrefConfig.orderIdOrgan) != null)
            {
                CompositeLesson = await LoadData.Instance.HandleGetListLessons(currentPage.ToString(), PlayerPrefs.GetString(PlayerPrefConfig.orderIdOrgan));
            }
            else
            {
                return;
            }
            Lessons = CompositeLesson.lessons;
            CurrentTotalPage = CompositeLesson.totalPage;
            DisplayListLesson();
        }
        async void LoadDataOrgans()
        {
            Organs = await LoadData.Instance.HandleGetListOrgans();
            DisplayListOrgan();
        }
        #endregion Call API

        #region Sub method
        GameObject CreateUIItem(string pathPrefabItem, Transform transformParent)
        {
            GameObject prefabItem = Resources.Load(pathPrefabItem) as GameObject;
            GameObject itemObject = Instantiate(prefabItem);
            itemObject.transform.SetParent(transformParent, false);
            return itemObject;
        }
        
        async void OnClickedItemOgran(string orderIdOrgan = " ")
        {
            ResetLessons();
            PlayerPrefs.SetString(PlayerPrefConfig.orderIdOrgan, orderIdOrgan);
            PlayerPrefs.Save();
            await LoadLessonsWithPageNumber();
        }

        void ResetLessons()
        {
            foreach (Transform item in contentLesson)
            {
                Destroy(item.gameObject);
            }
            btnLoadMore.SetActive(true);
            currentPage = 0;
        }

        void OnClickedItemLesson(string lessonId)
        {
            PlayerPrefs.SetString("lessonId", lessonId);
            StartCoroutine(LoadSceneAsync.LoadYourAsyncScene(SceneConfig.lessonDetail));
        }

        // waiting load image async
        IEnumerator LoadImageFromURL(string url, GameObject obj)
        {
            WWW www = new WWW(url);
            yield return www;
            if (obj != null)
            {
                obj.transform.GetChild(1).gameObject.SetActive(true);
                if (obj.transform.GetChild(1).GetComponent<Image>() != null)
                {
                    obj.transform.GetChild(0).gameObject.SetActive(false);
                    obj.transform.GetChild(1).GetComponent<Image>().sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
                }
            }
        }
        #endregion Sub method
    }
}

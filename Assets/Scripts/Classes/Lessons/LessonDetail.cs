using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class LessonDetail
{
    public string lessonTitle;
    public string lessonThumbnail;
    public string viewed;
    public string lessonObjectives;
    public string lessonId;
    public string size;
    public string createdBy;
    public string authorAvatar;
    public string authorName;
    public string createdDate;
    public string organId;
    public string modelId;
    public string modelFile;
    public string video;
    public string audio;
    public Label[] listLabel;
}

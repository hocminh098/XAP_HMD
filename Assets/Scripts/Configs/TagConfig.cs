using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TagConfig
{
    public static string ORGAN = "Organ";
    public static string LABEL_TAG = "Label";
    public static string ICON_TICK = "IconTick";
    public static List<string> syncIgnoreAvatar = new List<string>()
    {
        "SyncIgnoreAvatar",
        LABEL_TAG
    };
    public static string XROriginTag = "XROriginTag";
    public static string leftControllerTag = "LeftControllerTag";
    public static string rightControllerTag = "RightControllerTag";
    public static string notificationTag = "NotificationTag";
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

namespace Lessons
{
    public class LoadData : MonoBehaviour
    {
        #region Singleton
        private static LoadData instance;
        public static LoadData Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<LoadData>();
                }
                return instance;
            }
        }

        #endregion Singleton
        public static event Action onChangeUserInfor;
        public static event Action<Organ[]> onLoadOrgans;
        public static event Action<Lesson[], int> onLoadLessons;
        #region Private Method

        public async Task<Organ[]> HandleGetListOrgans()
        {
            try
            {
                APIResponse<Organ[]> listOrgansResponse = await UnityHttpClient.CallAPI<APIResponse<Organ[]>>(ApiConfig.GET_LIST_ORGANS, UnityWebRequest.kHttpVerbGET, null);

                if (listOrgansResponse.code == ApiConfig.SUCCESS_RESPONSE_CODE)
                {
                    return listOrgansResponse.data;
                }
                else
                {
                    throw new Exception(listOrgansResponse.message);
                }
            }
            catch (Exception exception)
            {
                StartCoroutine(Helper.DisplayNotification(exception.Message));
                return null;
            }
        }

        public async Task<CompositeLesson> HandleGetListLessons(string numberPage = "", string orderIdOrgan = "")
        {
            try
            {
                APIResponse<List<Lesson>> listLessonsResponse = await UnityHttpClient.CallAPI<APIResponse<List<Lesson>>>(
                                                            ApiConfig.GET_LIST_LESSON_BY_ORGAN + orderIdOrgan + StringConfig.offset + numberPage,
                                                            UnityWebRequest.kHttpVerbGET,
                                                            null);

                Debug.Log($"load lessons by orderID link: {ApiConfig.GET_LIST_LESSON_BY_ORGAN + orderIdOrgan + StringConfig.offset + numberPage}");
                if (listLessonsResponse.code == ApiConfig.SUCCESS_RESPONSE_CODE)
                {
                    CompositeLesson compositeLesson = new CompositeLesson();
                    compositeLesson.lessons = listLessonsResponse.data;
                    compositeLesson.totalPage = listLessonsResponse.meta.totalPage;
                    return compositeLesson;
                }
                else
                {
                    throw new Exception(listLessonsResponse.message);
                }
            }
            catch (Exception exception)
            {

                StartCoroutine(Helper.DisplayNotification(exception.Message));
                return null;
            }
        }

        #endregion Pravite Method

        #region Public Method

        #endregion Public Method
    }
}

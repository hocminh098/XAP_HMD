using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserInfo
{
    public string email;
    public string fullName;
    public string avatar;
    public string token;
}

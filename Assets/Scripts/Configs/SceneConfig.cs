using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SceneConfig 
{
    public static string experience = "Experience";
    public static string meetingExperience = "MeetingExperience";
    public static string meetingJoining = "MeetingJoining";
    public static string meetingStarting = "MeetingStarting";
    public static string home = "Home";
    public static string login = "Login";
    public static string lesson = "Lesson";
    public static string lessonDetail = "LessonDetail";
    public static string meetingCube = "HelloCube";
}

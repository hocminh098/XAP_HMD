﻿using UnityEngine;
using System.Collections;

namespace TalesFromTheRift
{
    public class CanvasNumberKeyboardASCII : MonoBehaviour
    {
        public CanvasKeyboard canvasKeyboard;

        public void OnKeyDown(GameObject kb)
        {
            if (kb.name == "DONE")
            {
                if (canvasKeyboard != null)
                {
                    canvasKeyboard.CloseKeyboard();
                }
            }
            else
            {
                if (canvasKeyboard != null)
                {
                    string s;
                    if (kb.name == "BACKSPACE")
                    {
                        s = "\x08";
                    }
                    else
                    {
                        s = kb.name;
                    }
                    canvasKeyboard.SendKeyString(s);
                }
            }
        }
    }
}
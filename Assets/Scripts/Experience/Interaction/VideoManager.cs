using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using System;

namespace YoutubePlayer
{
    public class VideoManager : MonoBehaviour
    {
        private static VideoManager instance;
        public static VideoManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<VideoManager>();
                }
                return instance;
            }
        }

        public GameObject mainViewVideo;
        // YoutubePlayer youtubePlayer;
        VideoPlayer videoPlayer;
        public Button btnControlVideo;
        public Button btnSkipPreVideo;
        public Button btnSkipNextVideo;
        public GameObject sliderControlVideo;
        public GameObject listMedia;
        public GameObject viewVideo;
        public GameObject panelLoading;
        public bool IsPlayingVideo { get; set; } = false;

        private void Awake()
        {
            videoPlayer = mainViewVideo.GetComponent<VideoPlayer>();
            videoPlayer.prepareCompleted += VideoPlayerPreparedCompleted;
            youtubePlayer = mainViewVideo.GetComponent<YoutubePlayer>();
        }

        void Update()
        {
            if (videoPlayer != null)
            {
                // sliderControlVideo.GetComponent<Slider>().value = (float)videoPlayer.time;

                // sliderControlVideo.GetComponent<Slider>().onValueChanged.AddListener((value) => { 
                //     videoPlayer.time = value;
                // });
                StartCoroutine(SetValueForSlider());
            }
        }

        IEnumerator SetValueForSlider()
        {
            sliderControlVideo.GetComponent<Slider>().onValueChanged.AddListener((value) =>
            {
                videoPlayer.time = value;
            });

            yield return new WaitForSeconds(2);

            sliderControlVideo.GetComponent<Slider>().value = (float)videoPlayer.time;
        }

        public void ShowVideo(MediaOrganItem dataItemVideo, GameObject itemMedia)
        {
            ResetVideo();

            listMedia.SetActive(false);
            viewVideo.SetActive(true);

            itemMedia.transform.GetChild(2).gameObject.SetActive(true);

            btnControlVideo.interactable = false;
            btnSkipPreVideo.interactable = false;
            btnSkipNextVideo.interactable = false;
            sliderControlVideo.SetActive(false);
            panelLoading.SetActive(true);

            youtubePlayer.youtubeUrl = dataItemVideo.VideoURL;

            Prepare();
        }

        void VideoPlayerPreparedCompleted(VideoPlayer source)
        {
            btnControlVideo.interactable = source.isPrepared;
            btnSkipPreVideo.interactable = source.isPrepared;
            btnSkipNextVideo.interactable = source.isPrepared;
            sliderControlVideo.SetActive(source.isPrepared);
            sliderControlVideo.GetComponent<Slider>().maxValue = (float)videoPlayer.length;
        }

        public async void Prepare()
        {
            Debug.Log("Loading video...");
            try
            {
                await youtubePlayer.PrepareVideoAsync();
                panelLoading.SetActive(false);
                Debug.Log("Loading video success!");
            }
            catch (Exception e)
            {
                videoPlayer = null;
                Debug.Log("ERROR video " + e);
            }
        }

        public void ControlVideo(bool _IsPlayingAudio)
        {
            IsPlayingVideo = _IsPlayingAudio;
            if (videoPlayer != null)
            {
                if (IsPlayingVideo)
                {
                    PlayVideo();
                    btnControlVideo.GetComponent<Image>().sprite = Resources.Load<Sprite>(PathResourceConfig.VIDEO_PAUSE_ICON);
                }
                else
                {
                    PauseVideo();
                    btnControlVideo.GetComponent<Image>().sprite = Resources.Load<Sprite>(PathResourceConfig.VIDEO_PLAY_ICON);
                }
            }
        }

        public void PlayVideo()
            {
                videoPlayer.Play();
            }
            public void PauseVideo()
            {
                videoPlayer.Pause();
            }
            public void ResetVideo()
            {
                if (videoPlayer != null)
                    videoPlayer.Stop();
                // videoPlayer = null;
                btnControlVideo.GetComponent<Image>().sprite = Resources.Load<Sprite>(PathResourceConfig.VIDEO_PLAY_ICON);
                MediaManager.Instance.DeleteAllIconTickOnItemMedia();
            }

        public void SkipPreVideo()
        {
            if (videoPlayer.time > MediaConfig.TIME_SKIP_DURATION)
                videoPlayer.time -= MediaConfig.TIME_SKIP_DURATION;
            else
                videoPlayer.time = 0f;
        }

        public void SkipNextVideo()
        {
            if ((videoPlayer.length - videoPlayer.time) > MediaConfig.TIME_SKIP_DURATION)
                videoPlayer.time += MediaConfig.TIME_SKIP_DURATION;
            else
                videoPlayer.time = 0f;
        }

        public void ExitVideo()
        {
            ResetVideo();
            listMedia.SetActive(true);
            viewVideo.SetActive(false);
        }

        void OnDestroy()
        {
            videoPlayer.prepareCompleted -= VideoPlayerPreparedCompleted;
        }
    }
}

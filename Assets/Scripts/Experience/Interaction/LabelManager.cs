using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LabelManager : MonoBehaviour
{
    private static LabelManager instance;
    public static LabelManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<LabelManager>();
            }
            return instance;
        }
    }

    private const float LONG_LINE_FACTOR = 3f;
    private int childCount;
    private string levelObject = "";
    private Vector3 centerPointParentObject;
    private List<GameObject> listLabelObjects = new List<GameObject>();
    private List<Transform> listChildrenTransform = new List<Transform>();
    List<GameObject> labelObjects = new List<GameObject>();

     // UI
    public Button btnLabel;
    private bool isShowingLabel;
    public bool IsShowingLabel
    {
        get
        {
            return isShowingLabel;
        }
        set
        {
            isShowingLabel = value;
            btnLabel.GetComponent<Image>().sprite = isShowingLabel ? Resources.Load<Sprite>(PathResourceConfig.LABEL_CLICKED_IMAGE) : Resources.Load<Sprite>(PathResourceConfig.LABEL_UNCLICKED_IMAGE);
        }
    }

    public void HandleLabelView(bool currentLabelStatus)
    {
        IsShowingLabel = currentLabelStatus;
        if (IsShowingLabel)
        {
            btnLabel.interactable = false;
            CreateLabel();
            btnLabel.interactable = true;
        }
        else
        {
            btnLabel.interactable = false;
            ClearLabel();
            btnLabel.interactable = true;
        }
    }

    public bool CheckAvailableLabel(GameObject obj)
    {
        if (StaticLesson.ListLabel.Length <= 0)
            return false;

        string levelObject = Helper.GetLevelObjectInLevelParent(obj);
        foreach (Label item in StaticLesson.ListLabel)
        {
            if (item.level == levelObject)
                return true;
        }
        return false;
    }

    public void CreateLabel()
    {
        ClearLabel();
        foreach (Label item in StaticLesson.ListLabel)
        {
            levelObject = Helper.GetLevelObjectInLevelParent(ARObjectManager.Instance.CurrentObject);
            if (item.level == levelObject)
            {
                GameObject tag = Instantiate(Resources.Load(PathResourceConfig.MODEL_TAG_LABEL) as GameObject);
                tag.tag = TagConfig.LABEL_TAG;
                tag.transform.SetParent(ARObjectManager.Instance.CurrentObject.transform, false);

                tag.transform.GetChild(1).localScale = tag.transform.GetChild(1).localScale / ARObjectManager.Instance.FactorScaleInitial;
                SetLabel(ARObjectManager.Instance.CurrentObject, tag, item);
                labelObjects.Add(tag);
            }
        }
    }

    public void ClearLabel()
    {
        foreach (GameObject label in labelObjects)
        {
            Destroy(label);
        }
        labelObjects.Clear();
    }

    public void SetLabel(GameObject childObject, GameObject tag, Label label)
    {
        GameObject line = tag.transform.GetChild(0).gameObject;
        GameObject labelName = tag.transform.GetChild(1).gameObject;
        labelName.transform.GetChild(0).GetComponent<Text>().text = label.labelName;

        Vector3 centerPointChildObject = Helper.CalculateBounds(childObject).center;
        Vector3 pointClickInObject = new Vector3(label.coordinates.x, label.coordinates.y, label.coordinates.z);
        Vector3 directionVector = (centerPointChildObject - pointClickInObject).normalized / ARObjectManager.Instance.FactorScaleInitial; ;

        labelName.transform.localPosition = pointClickInObject;

        line.GetComponent<LineRenderer>().SetVertexCount(2);
        line.GetComponent<LineRenderer>().SetPosition(0, directionVector * LONG_LINE_FACTOR);
        line.GetComponent<LineRenderer>().SetPosition(1, labelName.transform.localPosition);
        line.GetComponent<LineRenderer>().SetColors(Color.black, Color.black);
    }

    public void DestroyLabel()
    {
        foreach (GameObject label in listLabelObjects)
        {
            Destroy(label);
        }
        TagHandler.Instance.DeleteTags();
    }
}


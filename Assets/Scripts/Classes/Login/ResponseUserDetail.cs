using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ResponseUserDetail
{
    public int code;
    public string message;
    public DataUserDetail[] data;
}

[Serializable]
public class DataUserDetail
{
    public string userId;
    public string email;
    public string fullName;
    public string avatar;
    public string isActive;
}

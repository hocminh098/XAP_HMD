using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationManager : MonoBehaviour
{
    #region Singleton pattern
    private static RotationManager instance;
    public static RotationManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<RotationManager>();
            }
            return instance;
        }
    }
    #endregion Singleton pattern
    // public GameObject go_LeftHandController;
    // public GameObject go_RightHandController;

    float rotationRate = 70f;
    Vector2 previousPosition;
    Vector2 primary2DAxisValue;
    float deltaX;
    float deltaY;
    bool isFirstRotation = true;
    public GameObject go_LeftHandController;
    public GameObject go_RightHandController;
    public GameObject locomotionObject;

    public void Rotate()
    {
        primary2DAxisValue = TouchTrackPadManager.Instance.GetValueTouchPadInPrimary2DAxisRight() != Vector2.zero
        ? TouchTrackPadManager.Instance.GetValueTouchPadInPrimary2DAxisRight() : TouchTrackPadManager.Instance.GetValueTouchPadInPrimary2DAxisLeft();

        if ((primary2DAxisValue != Vector2.zero) && (XRRaycast.Instance.IsHoverObject(go_RightHandController) || XRRaycast.Instance.IsHoverObject(go_LeftHandController)))
        {
            if (isFirstRotation)
            {
                isFirstRotation = false;
                previousPosition = primary2DAxisValue;
            }
            else
            {
                locomotionObject.SetActive(false);
                deltaX = primary2DAxisValue.x - previousPosition.x;
                deltaY = primary2DAxisValue.y - previousPosition.y;
                ARObjectManager.Instance.OriginObject.transform.Rotate(deltaY * rotationRate, -deltaX * rotationRate, 0, Space.World);
                previousPosition = primary2DAxisValue;
            }
        }
        else
        {
            isFirstRotation = true;
            locomotionObject.SetActive(true);
        }
    }
}

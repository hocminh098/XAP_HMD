using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Linq;
// using YoutubePlayer;

public class ExperienceManager : MonoBehaviour
{
    public Button btnHold;
    public Button btnXray;
    public Button btnSeparate;
    public Button btnLabel;
    public Button btnPlayVideo;
    public Button btnSkipPreVideo;
    public Button btnSkipNextVideo;
    public Button btnPlayAudio;
    public Button btnSkipPreAudio;
    public Button btnSkipNextAudio;
    public Button btnExitVideo;
    public Button btnListMedia;
    public Button btnBack;

    void OnEnable()
    {
        SelectOrganManager.onClickNodeTree += OnClickNodeTree;
        SelectOrganManager.onSelectChildObject += OnSelectChildObject;
        ARObjectManager.onLoadedObjectAtRuntime += OnLoadedObjectAtRuntime;
    }

    void OnDisable()
    {
        SelectOrganManager.onClickNodeTree -= OnClickNodeTree;
        SelectOrganManager.onSelectChildObject -= OnSelectChildObject;
        ARObjectManager.onLoadedObjectAtRuntime -= OnLoadedObjectAtRuntime;
    }

    void Start()
    {
        LoadObjectModel();
        MediaManager.Instance.ListMediaOnPanel();
        InitEvents();
    }

    void Update()
    {
        ZoomManager.Instance.Zoom();
        RotationManager.Instance.Rotate();
        SelectOrganManager.Instance.SelectObject();
        DragManager.Instance.Drag();
        AllowInteraction();
    }

    void AllowInteraction()
    {
        btnLabel.interactable = (ARObjectManager.Instance.CurrentObject != null && 
                                LabelManager.Instance.CheckAvailableLabel(ARObjectManager.Instance.CurrentObject));
        btnXray.interactable = (ARObjectManager.Instance.CurrentObject != null);
        btnHold.interactable = (ARObjectManager.Instance.CurrentObject != null);
        btnSeparate.interactable = (ARObjectManager.Instance.CurrentObject != null && ARObjectManager.Instance.CurrentObject.transform.childCount > 1);                        
    }

    async void LoadObjectModel()
    {
        await ARObjectManager.Instance.LoadObjectAtRunTime();
    }

    void OnLoadedObjectAtRuntime(GameObject loadedObject)
    {
        GameObject objectInstance = Instantiate(loadedObject, ModelConfig.CENTER_POSITION, Quaternion.Euler(Vector3.zero));
        ARObjectManager.Instance.InitGameObject(objectInstance);
        LoadingEffectManager.Instance.ShowLoadingEffect(false);
        InitUI();
    }

    void InitUI()
    {
        btnPlayAudio.interactable = false;
        btnSkipNextAudio.interactable = false;
        btnSkipPreAudio.interactable = false;
    }

    void InitEvents()
    {
        btnHold.onClick.AddListener(HandleHoldObject);
        btnXray.onClick.AddListener(HandelXrayObject);
        btnSeparate.onClick.AddListener(HandelSeparateObject);
        // btnStartTree.onClick.AddListener(HandleResetObject);
        btnLabel.onClick.AddListener(HandleShowLabel);
        btnPlayVideo.onClick.AddListener(HandlePlayVideo);
        btnSkipPreVideo.onClick.AddListener(HandleSkipPreVideo);
        btnSkipNextVideo.onClick.AddListener(HandleSkipNextVideo);
        btnPlayAudio.onClick.AddListener(HandlePlayAudio);
        btnSkipPreAudio.onClick.AddListener(HandleSkipPreAudio);
        btnSkipNextAudio.onClick.AddListener(HandleSkipNextAudio);
        btnExitVideo.onClick.AddListener(HandleExitVideo);
        btnListMedia.onClick.AddListener(HandleExitVideo);
        btnBack.onClick.AddListener(HanldeBackToLessonDetail);
    }

    void HandleHoldObject()
    {
        DragManager.Instance.IsHolding = !DragManager.Instance.IsHolding;
    }

    void HandelXrayObject()
    {
        XrayManager.Instance.IsMakingXRay = !XrayManager.Instance.IsMakingXRay;
        XrayManager.Instance.HandleXrayView(XrayManager.Instance.IsMakingXRay);
    }

    void HandelSeparateObject()
    {
        SeparationManager.Instance.IsSeparating = !SeparationManager.Instance.IsSeparating;
        SeparationManager.Instance.HandleSeparate(SeparationManager.Instance.IsSeparating);
    }

    void HanldeBackToLessonDetail()
    {
        StartCoroutine(LoadSceneAsync.LoadYourAsyncScene(SceneConfig.lessonDetail));
    }

    void HandleShowLabel()
    {
        LabelManager.Instance.IsShowingLabel = !LabelManager.Instance.IsShowingLabel;
        LabelManager.Instance.HandleLabelView(LabelManager.Instance.IsShowingLabel);
    }

    void HandlePlayVideo()
    {
        VideoManager.Instance.IsPlayingVideo = !VideoManager.Instance.IsPlayingVideo;
        VideoManager.Instance.ControlVideo(VideoManager.Instance.IsPlayingVideo);
    }

    void HandlePlayAudio()
    {
        AudioManager.Instance.IsPlayingAudio = !AudioManager.Instance.IsPlayingAudio;
        AudioManager.Instance.ControlAudio(AudioManager.Instance.IsPlayingAudio);
    }

    void HandleSkipPreVideo()
    {
        VideoManager.Instance.SkipPreVideo();
    }

    void HandleSkipNextVideo()
    {
        VideoManager.Instance.SkipNextVideo();
    }

    void HandleExitVideo()
    {
        VideoManager.Instance.ExitVideo();
    }

    void HandleSkipPreAudio()
    {
        AudioManager.Instance.SkipPreAudio();
    }

    void HandleSkipNextAudio()
    {
        AudioManager.Instance.SkipNextAudio();
    }

    void OnClickNodeTree(string nodeName)
    {
        OnResetStatusFeature();
        // XrayManager.Instance.HandleXRayView(false);
        if (nodeName != ARObjectManager.Instance.CurrentObject.name)
        {
            GameObject selectedObject = GameObject.Find(nodeName);
            SelectOrganManager.Instance.DisplayAllChildOfSelectedObject(selectedObject);
            // Conjoined
            ARObjectManager.Instance.CurrentObject = selectedObject;
            SeparationManager.Instance.HandleSeparate(false);

            ARObjectManager.Instance.ChangeCurrentObject(selectedObject);
            SelectOrganManager.Instance.RemoveMultipleItemNodeTree(nodeName);
            // StartCoroutine(Helper.MoveObject(Camera.main.gameObject, Camera.main.transform.position));
        }
        ARObjectManager.Instance.OriginObject.transform.localRotation = Quaternion.Euler(Vector3.zero);
    }

    void OnResetStatusFeature()
    {
        LabelManager.Instance.IsShowingLabel = false;
        LabelManager.Instance.HandleLabelView(LabelManager.Instance.IsShowingLabel);

        SeparationManager.Instance.IsSeparating = false;
        SeparationManager.Instance.HandleSeparate(SeparationManager.Instance.IsSeparating);

        DragManager.Instance.IsHolding = false;
    }

    void OnSelectChildObject(GameObject selectedObject)
    {
        OnResetStatusFeature();
        SelectOrganManager.Instance.DisplaySelectedObject(selectedObject, ARObjectManager.Instance.CurrentObject);
        ARObjectManager.Instance.ChangeCurrentObject(selectedObject);
        SelectOrganManager.Instance.CreateChildNodeUI(selectedObject.name);
    }
}

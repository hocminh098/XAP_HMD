using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    private static AudioManager instance;
    public static AudioManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<AudioManager>();
            }
            return instance;
        }
    }

    public GameObject mainViewAudio;
    AudioSource audioPlayer;
    AudioClip audioClip;
    public Button btnControlAudio;
    public Button btnSkipNextAudio;
    public Button btnSkipPreAudio;
    public Slider sliderAudio;
    public Text txtNameAudio;
    public GameObject PanelLoading;
    public bool IsPlayingAudio { get; set; } = false;

    private void Awake()
    {
        audioPlayer = mainViewAudio.AddComponent<AudioSource>();
    }

    void Update()
    {
        if (audioPlayer != null)
        {
            StartCoroutine(SetValueForSlider());
        }
    }

    IEnumerator SetValueForSlider()
    {
        sliderAudio.onValueChanged.AddListener((value) =>
        {
            audioPlayer.time = value;
        });
        yield return new WaitForSeconds(3);

        sliderAudio.value = (float)audioPlayer.time;
    }

    public void ChangeTimeAudio(float time)
    {
        audioPlayer.time = time;
    }

    async void GetAudioClip(string audioURL)
    {
        try
        {
            audioPlayer.clip = await UnityHttpClient.GetAudioClip(audioURL);
            Debug.Log("Loading audio success!");
            PanelLoading.SetActive(false);

            sliderAudio.maxValue = (float)audioPlayer.clip.length;
            btnControlAudio.interactable = true;
            btnSkipNextAudio.interactable = true;
            btnSkipPreAudio.interactable = true;
        }
        catch (Exception exception)
        {
            StartCoroutine(Helper.DisplayNotification(exception.Message));
        }
    }

    public void ShowAudio(MediaOrganItem dataItemVideo, GameObject itemMedia)
    {
        if (audioPlayer != null)
            ResetAudio();

        btnControlAudio.interactable = false;
        PanelLoading.SetActive(true);
        txtNameAudio.text = dataItemVideo.MediaName;
        itemMedia.transform.GetChild(2).gameObject.SetActive(true);

        GetAudioClip(dataItemVideo.AudioURL);
    }

    public void ControlAudio(bool _IsPlayingAudio)
    {
        IsPlayingAudio = _IsPlayingAudio;
        if (audioPlayer != null)
        {
            if (IsPlayingAudio)
            {
                PlayAudio();
                btnControlAudio.GetComponent<Image>().sprite = Resources.Load<Sprite>(PathResourceConfig.AUDIO_PAUSE_ICON);
            }
            else
            {
                PauseAudio();
                btnControlAudio.GetComponent<Image>().sprite = Resources.Load<Sprite>(PathResourceConfig.AUDIO_PLAY_ICON);
            }
        }
    }

    public void PauseAudio()
    {
        audioPlayer.Pause();
    }

    public void PlayAudio()
    {
        audioPlayer.Play();
    }

    public void ResetAudio()
    {
        audioPlayer.Stop();
        // audioPlayer = null;
        btnControlAudio.GetComponent<Image>().sprite = Resources.Load<Sprite>(PathResourceConfig.AUDIO_PLAY_ICON);
        MediaManager.Instance.DeleteAllIconTickOnItemMedia();
    }

    public void SkipPreAudio()
    {
        if (audioPlayer.time > MediaConfig.TIME_SKIP_DURATION)
            audioPlayer.time -= MediaConfig.TIME_SKIP_DURATION;
        else
            audioPlayer.time = 0f;
    }

    public void SkipNextAudio()
    {
        if ((audioPlayer.clip.length - audioPlayer.time) > MediaConfig.TIME_SKIP_DURATION)
            audioPlayer.time += MediaConfig.TIME_SKIP_DURATION;
        else
            audioPlayer.time = 0f;
    }
}

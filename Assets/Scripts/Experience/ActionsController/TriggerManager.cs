// using System;
// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;
// using UnityEngine.InputSystem;
// using UnityEngine.UI;
// using UnityEngine.XR;

// public class TriggerManager : MonoBehaviour
// {
//     #region Singleton pattern 
//         private static TriggerManager instance;
//         public static TriggerManager Instance
//         {
//             get
//             {
//                 if (instance == null)
//                 {
//                     instance = FindObjectOfType<TriggerManager>();
//                 }
//                 return instance;
//             }
//         }
//     #endregion Singleton pattern
//      #region Variable Common
//         [SerializeField]
//         private InputActionReference m_LeftController;
//         [SerializeField]
//         private InputActionReference m_RightController;
//         public InputActionReference leftController { get => m_LeftController ; set => m_LeftController = value; }
//         public InputActionReference rightController { get => m_RightController ; set => m_RightController = value; }

//         Type lastActiveType = null;

//     #endregion Variable Common

//     protected void OnEnable()
//     {
//         rightController.action.Enable();
//         leftController.action.Enable();
//     }

//     protected void OnDisable()
//     {
//         rightController.action.Disable();
//         leftController.action.Disable();
//     }
//      public bool IsPressTriggerSameTimeOfController()
//     {
//         if ((rightController != null && rightController.action != null && rightController.action.enabled && rightController.action.controls.Count > 0)
//         && (leftController != null && leftController.action != null && leftController.action.enabled && leftController.action.controls.Count > 0))
//         {
//             Type typeToUseLeft = null;
//             Type typeToUseRight = null;

//             if (rightController.action.activeControl != null)
//             {
//                 typeToUseRight = rightController.action.activeControl.valueType;
//             }
            
//             if (leftController.action.activeControl != null)
//             {
//                 typeToUseLeft = leftController.action.activeControl.valueType;
//             }


//             if (typeToUseLeft == typeof(float) && typeToUseRight == typeof(float))
//             {
//                 return true;
//             }
//         }
//         return false;
//     }

//     public bool IsPressTriggeredLeftController()
//     {
//         if ((leftController != null && leftController.action != null && leftController.action.enabled && leftController.action.controls.Count > 0))
//         {
//             Type typeToUseLeft = null;

//             if (leftController.action.activeControl != null)
//             {
//                 typeToUseLeft = leftController.action.activeControl.valueType;
//             }

//             if (typeToUseLeft == typeof(float))
//             {
//                 return true;
//             }
//         }
//         return false;
//     }
//     public bool IsPressTriggeredRightController()
//     {
//         if ((rightController != null && rightController.action != null && rightController.action.enabled && rightController.action.controls.Count > 0))
//         {
//             Type typeToUseLRight = null;

//             if (rightController.action.activeControl != null)
//             {
//                 typeToUseLRight = rightController.action.activeControl.valueType;
//             }

//             if (typeToUseLRight == typeof(float))
//             {
//                 return true;
//             }
//         }
//         return false;
//     }
// }

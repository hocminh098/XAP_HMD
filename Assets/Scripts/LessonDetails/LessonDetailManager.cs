using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace LessonDetails
{
    public class LessonDetailManager : MonoBehaviour
    {
        private static LessonDetailManager instance;
        public static LessonDetailManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<LessonDetailManager>();
                }
                return instance;
            }
        }

        public Text txtMainNavigation;
        // public Button btnLogout;
        public Button btnStartMeeting;
        public Button btnStartLesson;

        public Text txtNameAuthor;
        public Text txtLessonID;
        public Text txtView;
        public Text txtTime;
        public Text txtContent;
        public GameObject modelViewer;
        public Button btnHome;
        // public GameObject lessonInfoPanel;
        // public GameObject meetingInfoPanel;
        // public GameObject meetingProcessController;
        // public GameObject photonVoiceManager;

        void Start()
        {
            // SetInfoUser();
            LoadDataLessonDetail();
            SetActions();
        }

        void Update()
        {
            
        }

        // void SetInfoUser()
        // {
        //     txtMainNavigation.text = StringConfig.welcome + PlayerPrefs.GetString(PlayerPrefs.fullName);
        // }

        void SetActions()
        {
            btnStartLesson.onClick.AddListener(StartExperience);
            btnStartMeeting.onClick.AddListener(StartMeeting);
            btnHome.onClick.AddListener(BackToHome);
        }
        
        async void LoadDataLessonDetail()
        {
            StaticLesson.SetValueForStaticLesson(await LoadData.Instance.HandleGetLessonDetail(PlayerPrefs.GetString(PlayerPrefConfig.lessonId)));
            LoadLessonDetail();
        }

        void BackToHome()
        {
            PlayerPrefs.DeleteKey(PlayerPrefConfig.lessonId);
            StartCoroutine(LoadSceneAsync.LoadYourAsyncScene(SceneConfig.home));
        }

        void LoadLessonDetail()
        {
            txtNameAuthor.text = StaticLesson.AuthorName;
            txtLessonID.text = StaticLesson.LessonId;
            txtView.text = StaticLesson.Viewed;
            txtTime.text = System.DateTime.Parse(StaticLesson.CreatedDate).ToString();
            txtContent.text = StaticLesson.LessonObjectives;

            string imageURL = ApiConfig.DOMAIN_SERVER + StaticLesson.LessonThumbnail;
            Image targetImage = modelViewer.transform.GetChild(0).GetComponent<Image>();
            Davinci.get().load(imageURL).setFadeTime(0).setCached(true).into(targetImage).start();
            modelViewer.transform.GetChild(1).gameObject.SetActive(false);

            // StartCoroutine(LoadImageFromURL(ApiConfig.DOMAIN_SERVER + StaticLesson.LessonThumbnail, modelViewer));
        }

        void SignOut()
        {
            PlayerPrefs.DeleteAll();
        }

        void StartExperience()
        {
            StartCoroutine(LoadSceneAsync.LoadYourAsyncScene(SceneConfig.experience));
        }

        void StartMeeting()
        {
            // lessonInfoPanel.SetActive(false);
            // meetingInfoPanel.SetActive(true);
            // if (meetingProcessController.activeSelf)
            // {
            //     MeetingStartingHandler.Instance.InitActions();
            // }
            // else
            // {
            //     meetingProcessController.SetActive(true);
            //     photonVoiceManager.SetActive(true);
            // }
        }

        IEnumerator LoadImageFromURL(string url, GameObject obj)
        {
            WWW www = new WWW(url);
            yield return www;
            obj.transform.GetChild(1).gameObject.SetActive(true);
            if (obj.transform.GetChild(1).GetComponent<Image>() != null)
            {
                obj.transform.GetChild(0).gameObject.SetActive(false);
                obj.transform.GetChild(1).GetComponent<Image>().sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
            }
        }
    }
}

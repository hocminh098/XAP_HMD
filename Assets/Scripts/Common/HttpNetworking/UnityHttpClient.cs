using System;
using System.Collections;
using System.Text;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Threading.Tasks;

public static class UnityHttpClient 
{
    public static async Task<TResponseType> CallAPI<TResponseType>(string url, string method, object requestData = null)
    {
        try
        {
            // convert request data to byte array
            UnityWebRequest www = new UnityWebRequest(ApiConfig.DOMAIN_SERVER + url, method);
            if (requestData != null)
            {
                string requestDataString = JsonUtility.ToJson(requestData);
                byte[] requestDataBytes = Encoding.UTF8.GetBytes(requestDataString);
                www.uploadHandler = (UploadHandler)new UploadHandlerRaw(requestDataBytes);
            }
            www.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
            www.SetRequestHeader(ApiConfig.CONTENT_TYPE_KEY, ApiConfig.JSON_CONTENT_TYPE_VALUE);
            www.SetRequestHeader(ApiConfig.AUTHORIZATION_KEY, PlayerPrefs.GetString(PlayerPrefConfig.token));
            var operation = www.SendWebRequest();
            while (!operation.isDone)
            {
                await Task.Yield();
            }
            if (www.result == UnityWebRequest.Result.ConnectionError)
            {
                // Only for error connection. Bad request will be handled (show message)
                throw new Exception(www.error);
            }
            TResponseType responseData = JsonUtility.FromJson<TResponseType>(www.downloadHandler.text);
            return responseData;
        }
        catch (Exception exception)
        {
            throw exception;
        }
    }

    public static async Task<AudioClip> GetAudioClip(string audioURL)
    {
        try
        {
            UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(ApiConfig.DOMAIN_SERVER + audioURL, AudioType.MPEG);

            var operation = www.SendWebRequest();
            while (!operation.isDone)
            {
                await Task.Yield();
            }
            // ((DownloadHandlerAudioClip)www.downloadHandler).streamAudio = true;
            if (www.result == UnityWebRequest.Result.ConnectionError)
            {
                throw new Exception(www.error);
            }
            AudioClip audioClip = DownloadHandlerAudioClip.GetContent(www);
            Debug.Log($"{audioClip}");
            return audioClip;
        }
        catch (Exception exception)
        {
            throw exception;
        }

    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Login
{
    public class LoadData : MonoBehaviour
    {
        #region Singleton
        private static LoadData instance;
        public static LoadData Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<LoadData>();
                }
                return instance;
            }
        }
        public static event Action onUserLogin;
        public static event Action<string> onLoginFailed;

        #endregion Singleton

        #region Private Method
        async void HandleLoginUser(string email, string password)
        {
            try
            {
                RequestLogin requestData = new RequestLogin();
                requestData.email = email;
                requestData.password = password;

                APIResponse<UserInfo[]> userDetailResponse = await UnityHttpClient.CallAPI<APIResponse<UserInfo[]>>(ApiConfig.POST_LOGIN, UnityWebRequest.kHttpVerbPOST, requestData);
                if (userDetailResponse.code == ApiConfig.SUCCESS_RESPONSE_CODE)
                {
                    PlayerPrefs.SetString(PlayerPrefConfig.email, userDetailResponse.data[0].email);
                    PlayerPrefs.SetString(PlayerPrefConfig.token, userDetailResponse.data[0].token);
                    PlayerPrefs.SetString(PlayerPrefConfig.fullName, userDetailResponse.data[0].fullName);
                    PlayerPrefs.SetString(PlayerPrefConfig.avatar, userDetailResponse.data[0].avatar);
                    PlayerPrefs.Save();

                    onUserLogin?.Invoke();
                }
                else
                {
                    throw new Exception(userDetailResponse.message);
                }

            }
            catch (Exception exception)
            {
                onLoginFailed?.Invoke(exception.Message);
            }
        }
        #endregion Pravite Method

        #region Public Method

        public void LoginUser(string email, string password)
        {
            HandleLoginUser(email, password);
        }

        #endregion Public Method
    }
}

// using System;
// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;
// using UnityEngine.InputSystem;
// using UnityEngine.UI;
// using UnityEngine.XR;

// public class TouchTrackPadManager : MonoBehaviour
// {
//         #region Singleton pattern
//         private static TouchTrackPadManager instance;
//         public static TouchTrackPadManager Instance
//         {
//             get
//             {
//                 if (instance == null)
//                 {
//                     instance = FindObjectOfType<TouchTrackPadManager>();
//                 }
//                 return instance;
//             }
//         }
//         #endregion Singleton pattern

//         #region Variable Common
//             [SerializeField]
//             private InputActionReference m_LeftController;
//             [SerializeField]
//             private InputActionReference m_RightController;
//             public InputActionReference leftController { get => m_LeftController ; set => m_LeftController = value; }
//             public InputActionReference rightController { get => m_RightController ; set => m_RightController = value; }

//         #endregion Variable Common

//         #region Public Method
//             protected void OnEnable()
//             {
//                 rightController.action.Enable();
//                 leftController.action.Enable();
//             }

//             protected void OnDisable()
//             {
//                 leftController.action.Disable();
//                 rightController.action.Disable();
//             }
            
//             public Vector2 GetValueTouchPadInPrimary2DAxisRight()
//             {
//                 if (rightController != null && rightController.action != null && rightController.action.enabled)
//                 {
//                    return rightController.action.ReadValue<Vector2>();
//                 }
                
//                 return Vector2.zero;
//             }

//             public Vector2 GetValueTouchPadInPrimary2DAxisLeft()
//             {
//                 if (leftController != null && leftController.action != null && leftController.action.enabled)
//                 {
//                     return leftController.action.ReadValue<Vector2>();
//                 }
//                 return Vector2.zero;
//             }
  
//         #endregion Public Method
// }

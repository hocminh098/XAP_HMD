using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
// using UnityEngine.XR.Interaction.Toolkit;

public class DragManager : MonoBehaviour
{
     #region Singleton pattern
    private static DragManager instance;
    public static DragManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<DragManager>();
            }
            return instance;
        }
    }
    #endregion Singleton pattern

    public GameObject go_LeftHandController;
    public GameObject go_RightHandController;
    public Button btnHold;
    private bool isHolding;
    public bool IsHolding
    {
        get
        {
            return isHolding;
        }
        set
        {
            isHolding = value;
            btnHold.GetComponent<Image>().sprite = isHolding ? Resources.Load<Sprite>(PathResourceConfig.HOLD_CLICKED_IMAGE) : Resources.Load<Sprite>(PathResourceConfig.HOLD_UNCLICKED_IMAGE);
        }
    }

    bool isMoved = false;
    Vector3 mOffset;
    float mZCoord;
    Vector3 pointRayCastMoveOnRight;
    Vector3 pointRayCastMoveOnLeft;
    RayCastObject currentHoverRayCastObject;


    public void Drag()
    {
        currentHoverRayCastObject = XRRaycast.Instance.GetObjectRayCast(go_RightHandController) != null
        ? XRRaycast.Instance.GetObjectRayCast(go_RightHandController) : XRRaycast.Instance.GetObjectRayCast(go_LeftHandController);

        if (currentHoverRayCastObject != null)
        {
            if (!IsHolding) // not click holding -> drag parent object
            {
                currentHoverRayCastObject.HoverObject = ARObjectManager.Instance.OriginObject;
            }

            if ((currentHoverRayCastObject.RaycastPosition != Vector3.zero) && (TriggerManager.Instance.IsPressTriggeredRightController() || TriggerManager.Instance.IsPressTriggeredLeftController()))
            {
                if (!isMoved)
                {
                    mZCoord = currentHoverRayCastObject.HoverObject.transform.position.z;
                    mOffset = currentHoverRayCastObject.HoverObject.transform.position - SyncPositionWithAxisZCoord(currentHoverRayCastObject.RaycastPosition, mZCoord);
                    isMoved = true;
                }
                else
                {
                    currentHoverRayCastObject.HoverObject.transform.position = SyncPositionWithAxisZCoord(currentHoverRayCastObject.RaycastPosition, mZCoord) + mOffset;
                }
            }
            else
            {
                isMoved = false;
            }
        }
        else
        {
            return;
        }
    }

    /// <summary>
    /// Keep Axis Z of object when drag
    /// </summary>
    /// <param name="oldPosition"></param>
    /// <param name="zCoord"></param>
    /// <returns></returns>
    public Vector3 SyncPositionWithAxisZCoord(Vector3 position, float zCoord)
    {
        position.z = zCoord;
        return position;
    }

    public void UpdateUIForHoldingBtn(bool _isHoldingXRay)
    {
        IsHolding = _isHoldingXRay;
        if (IsHolding)
        {
            btnHold.GetComponent<Image>().sprite = Resources.Load<Sprite>(PathResourceConfig.HOLD_CLICKED_IMAGE);
        }
        else
        {
            btnHold.GetComponent<Image>().sprite = Resources.Load<Sprite>(PathResourceConfig.HOLD_UNCLICKED_IMAGE);
        }
    }
}

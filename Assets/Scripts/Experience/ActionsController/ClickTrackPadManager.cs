// using System;
// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;
// using UnityEngine.InputSystem;
// using UnityEngine.UI;
// using UnityEngine.XR;

// public class ClickTrackPadManager : MonoBehaviour
// {
//     #region Singleton pattern
//         private static ClickTrackPadManager instance;
//         public static ClickTrackPadManager Instance
//         {
//             get
//             {
//                 if (instance == null)
//                 {
//                     instance = FindObjectOfType<ClickTrackPadManager>();
//                 }
//                 return instance;
//             }
//         }
//     #endregion Singleton pattern

//     #region Variable Common
//         [SerializeField]
//         private InputActionReference m_LeftController;
//         [SerializeField]
//         private InputActionReference m_RightController;
//         public InputActionReference leftController { get => m_LeftController ; set => m_LeftController = value; }
//         public InputActionReference rightController { get => m_RightController ; set => m_RightController = value; }

//     #endregion Variable Common

//     #region Public Method
//         protected void OnEnable()
//         {
//             rightController.action.Enable();
//             leftController.action.Enable();
//         }

//         protected void OnDisable()
//         {
//             leftController.action.Disable();
//             rightController.action.Disable();
//         }

//         public bool IsClickTrackPadRightController()
//         {
//             if ((rightController != null && rightController.action != null && rightController.action.enabled && rightController.action.controls.Count > 0))
//             {
//                 Type typeToUseLRight = null;

//                 if (rightController.action.activeControl != null)
//                 {
//                     typeToUseLRight = rightController.action.activeControl.valueType;
//                 }

//                 if (typeToUseLRight == typeof(float))
//                 {
//                     return true;
//                 }
//             }
//             return false;
//         }

//         public bool IsClickTrackPadLeftController()
//         {
//             if ((leftController != null && leftController.action != null && leftController.action.enabled && leftController.action.controls.Count > 0))
//             {
//                 Type typeToUseLRight = null;

//                 if (leftController.action.activeControl != null)
//                 {
//                     typeToUseLRight = leftController.action.activeControl.valueType;
//                 }

//                 if (typeToUseLRight == typeof(float))
//                 {
//                     return true;
//                 }
//             }
//             return false;
//         }

//     #endregion Public Method
// }

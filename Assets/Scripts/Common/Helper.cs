using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// using TalesFromTheRift;
using UnityEngine;
using UnityEngine.UI;
using System;
using Random = UnityEngine.Random;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class Helper
{
    public static List<Vector3> GetListOfInitialPositionOfChildren(GameObject obj)
    {
        int childCount = obj.transform.childCount;

        List<Vector3> listchildrenOfOriginPosition = new List<Vector3>();

        for (int i = 0; i < childCount; i++)
        {
            listchildrenOfOriginPosition.Add(obj.transform.GetChild(i).localPosition);
        }

        return listchildrenOfOriginPosition;
    }

    public static void ResetFeatureEnableOnObject()
    {
        XrayManager.Instance.IsMakingXRay = false;
        XrayManager.Instance.HandleXRayView(XrayManager.Instance.IsMakingXRay);

        SeparationManager.Instance.IsSeparating = false;
        SeparationManager.Instance.HandleSeparate(SeparationManager.Instance.IsSeparating);

        DragManager.Instance.IsHolding = false;
    }

    public static IEnumerator MoveObject(GameObject moveObject, Vector3 targetPosition)
    {
        float timeSinceStarted = 0f;
        while (true)
        {
            timeSinceStarted += Time.deltaTime;
            moveObject.transform.position = Vector3.Lerp(moveObject.transform.position, targetPosition, timeSinceStarted);
            if (moveObject.transform.position == targetPosition)
            {
                yield break;
            }
            yield return null;
        }
    }

    public static IEnumerator DisplayNotification(string message)
    {
        GameObject notificationObject = null;
        Transform[] objs = Resources.FindObjectsOfTypeAll<Transform>() as Transform[];
        for (int i = 0; i < objs.Length; i++)
        {
            if (objs[i].hideFlags == HideFlags.None)
            {
                if (objs[i].CompareTag(TagConfig.notificationTag))
                {
                    notificationObject = objs[i].gameObject;
                    break;
                }
            }
        }
        if (notificationObject != null)
        {
            notificationObject.transform.GetChild(0).gameObject.GetComponent<Text>().text = message;
            notificationObject.SetActive(true);
            yield return new WaitForSeconds(4f);
            notificationObject.SetActive(false);
        }
    }

    public static Vector3 GetRandomPositionOnPlane(GameObject planeObject)
    {
        Bounds planeBoundary = CalculateBounds(planeObject);
        float xPosition = Random.Range(-planeBoundary.size.x / 2, planeBoundary.size.x / 2);
        float zPosition = Random.Range(-planeBoundary.size.z / 2, planeBoundary.size.z / 2);
        Vector3 positionOnPlane = planeObject.transform.position + new Vector3(xPosition, planeObject.transform.position.y, zPosition);
        return positionOnPlane;
    }

    public static string FormatTime(float time)
    {
        int minutes = (int)time / 60;
        int seconds = (int)time - 60 * minutes;
        return string.Format("{0:00}:{1:00}", minutes, seconds);
    }


    public static Bounds CalculateBounds(GameObject obj)
    {
        var renderers = obj.GetComponentsInChildren<Renderer>();
        if (renderers.Length <= 0)
        {
            return new Bounds(Vector3.zero, Vector3.zero);
        }
        Bounds bounds = renderers[0].bounds;
        for (var i = 1; i < renderers.Length; i++)
        {
            bounds.Encapsulate(renderers[i].bounds);
        }
        return bounds;
    }

    public static string GetLevelObjectInLevelParent(GameObject obj)
    {
        Transform currentObjectTransform = obj.transform;
        Transform rootTransform = currentObjectTransform.root;
        Transform parentTransform = null;
        string levelObject = StringConfig.levelParentObjectAppend;
        List<string> ListLevel = new List<string>();

        // Get List level object inside - outside, sub - root
        while (currentObjectTransform != rootTransform)
        {
            ListLevel.Add(currentObjectTransform.transform.GetSiblingIndex().ToString());
            parentTransform = currentObjectTransform.parent;
            currentObjectTransform = parentTransform;
        }

        // append string level invers level
        for (int i = ListLevel.Count - 1; i >= 0; i--)
        {
            if (i == 0)
                levelObject += ListLevel[i];
            else
                levelObject += ListLevel[i] + StringConfig.letterAppend;
        }
        return levelObject != StringConfig.levelParentObjectAppend ? levelObject : StringConfig.levelParentObject;
    }

    public static string ShortString(string inputString, int maxSize)
    {
        return inputString != null && inputString.Length > maxSize
                ? inputString.Substring(0, maxSize) + StringConfig.DOT_SHORT_STRING
                : inputString;
    }
}

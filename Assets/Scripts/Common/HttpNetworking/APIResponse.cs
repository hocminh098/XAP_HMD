using System;
using UnityEngine;

[Serializable]
public class APIResponse<T>
{
    public int code;
    public string message;
    public MetaData meta;
    public T data;
}

[Serializable]
public class MetaData
{
    public int page;
    public int pageSize;
    public int totalPage;
    public int totalElements;
    public int nextPage;
}

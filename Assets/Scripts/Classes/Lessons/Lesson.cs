using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Lesson
{
    public string lessonId;
    public string lessonTitle;
    public string lessonThumbnail;
    public string isActive;
    public string isPublic;
}

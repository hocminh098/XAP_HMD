using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectOrganManager : MonoBehaviour
{
    #region Singleton pattern
    private static SelectOrganManager instance;
    public static SelectOrganManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<SelectOrganManager>();
            }
            return instance;
        }
    }
    #endregion Singleton pattern
    public GameObject go_LeftHandController;
    public GameObject go_RightHandController;
    RayCastObject currentHoverRayCastObject;
    Vector3 positionCamera;

    public Transform rootTreeTransform;
    public static event Action onResetObject;
    public static event Action<string> onClickNodeTree;
    public static event Action<GameObject> onSelectChildObject;


    // public void SelectObject()
    // {
    //     currentHoverRayCastObject = XRRaycast.Instance.GetObjectRayCast(go_RightHandController) != null 
    //     ? XRRaycast.Instance.GetObjectRayCast(go_RightHandController) : XRRaycast.Instance.GetObjectRayCast(go_LeftHandController);

    //     if (currentHoverRayCastObject != null && 
    //     (ClickTrackPadManager.Instance.IsClickTrackPadRightController() || ClickTrackPadManager.Instance.IsClickTrackPadLeftController()))
    //     {
    //         DisplaySelectedObject(currentHoverRayCastObject.HoverObject, VRObjectManager.Instance.CurrentObject);
    //         ARObjectManager.Instance.ChangeCurrentObject(currentHoverRayCastObject.HoverObject);
    //         Debug.Log("current object - " + VRObjectManager.Instance.CurrentObject);
    //     }
    //     else
    //     {
    //         return;
    //     }
    // }

    void OnEnable()
    {
        ARObjectManager.onInitOrganSuccessfully += OnInitOrganSuccessfully;
    }

    void OnDisable()
    {
        ARObjectManager.onInitOrganSuccessfully -= OnInitOrganSuccessfully;
    }

    void OnInitOrganSuccessfully()
    {
        CreateChildNodeUI(ARObjectManager.Instance.OriginObject.name, true);
    }

    public void CreateChildNodeUI(string name, bool isHeaderTree = false)
    {
        GameObject nodePrefab = isHeaderTree ? (Resources.Load(PathResourceConfig.PATH_UI_NODE_TREE_HEADER) as GameObject) : (Resources.Load(PathResourceConfig.PATH_UI_NODE_TREE_BODY) as GameObject);
        GameObject childNode = Instantiate(nodePrefab);
        childNode.transform.SetParent(rootTreeTransform, false);
        childNode.transform.GetChild(1).GetComponent<Text>().text = name;
        childNode.name = name;
        childNode.transform.GetComponent<Button>().onClick.AddListener(delegate { OnClickedNodeTree(childNode.name); });
    }


    public void OnClickedNodeTree(String nameSelectedObject)
    {
        onClickNodeTree?.Invoke(nameSelectedObject);
    }

    void Start()
    {
        positionCamera = Camera.main.transform.position;
    }

    public void SelectObject()
    {
        currentHoverRayCastObject = XRRaycast.Instance.GetObjectRayCast(go_RightHandController) != null
        ? XRRaycast.Instance.GetObjectRayCast(go_RightHandController) : XRRaycast.Instance.GetObjectRayCast(go_LeftHandController);

        if ((currentHoverRayCastObject != null) && (ClickTrackPadManager.Instance.IsClickTrackPadRightController() || ClickTrackPadManager.Instance.IsClickTrackPadLeftController()))
        {
            onSelectChildObject?.Invoke(currentHoverRayCastObject.HoverObject);
        }
    }

    public void DisplaySelectedObject(GameObject selectedObject, GameObject parentObject)
    {
        foreach (Transform child in parentObject.transform)
        {
            if (child.gameObject != selectedObject)
            {
                child.gameObject.SetActive(false);
            }
        }
    }

    // public void HandleClickNodeTree(string selectedObjectName)
    // {
    //     GameObject selectedObject = GameObject.Find(selectedObjectName);
    //     DisplayAllChildOfSelectedObject(selectedObject);
    //     RemoveMultipleItemNodeTree(selectedObjectName);
    //     ARObjectManager.Instance.ChangeCurrentObject(selectedObject);
    //     StartCoroutine(Helper.MoveObject(Camera.main.gameObject, positionCamera));
    // }

    public void ClearAllNodeTree()
    {
        for (int i = 1; i < rootTreeTransform.transform.childCount; i++)
        {
            Destroy(rootTreeTransform.transform.GetChild(i).gameObject);
        }
    }
    public void RemoveMultipleItemNodeTree(string nameSelectedObject)
    {
        bool isDeleted = false;
        for (int i = 0; i < rootTreeTransform.transform.childCount; i++)
        {
            if (rootTreeTransform.transform.GetChild(i).gameObject.name == nameSelectedObject)
            {
                isDeleted = true;
                continue;
            }

            if (isDeleted)
            {
                Destroy(rootTreeTransform.transform.GetChild(i).gameObject);
            }
        }
    }

    public void DisplayAllChildOfSelectedObject(GameObject seletedObject)
    {
        int childCount = seletedObject.transform.childCount;
        if (childCount > 0)
        {
            for (int i = 0; i < childCount; i++)
            {
                if (seletedObject.transform.GetChild(i).gameObject.tag == TagConfig.LABEL_TAG)
                {
                    seletedObject.SetActive(true);
                    return;
                }
                else
                {
                    DisplayAllChildOfSelectedObject(seletedObject.transform.GetChild(i).gameObject);
                }
            }
        }
        else
        {
            seletedObject.SetActive(true);
            return;
        }
    }

    public void InitTreeNodeByObject(GameObject currentObject)
    {
        List<String> listParentName = new List<String>();
        while (currentObject.name != ARObjectManager.Instance.OriginObject.name)
        {
            listParentName.Add(currentObject.name);
            currentObject = currentObject.transform.parent.gameObject;
        }

        for (int i = listParentName.Count - 1; i >= 0; i--)
        {
            CreateChildNodeUI(listParentName[i]);
        }
    }

    public void GetNestParent(GameObject obj, List<String> listNameParent)
    {
        listNameParent.Add(obj.name);
        if (obj.transform.parent != ARObjectManager.Instance.OriginObject)
        {
            GetNestParent(obj.transform.parent.gameObject, listNameParent);
        }
    }
}


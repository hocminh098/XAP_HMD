using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Debug = UnityEngine.Debug;
using System.Threading.Tasks;
using System.Net;
using System.IO;
// using TriLibCore;
using System.Linq;

public class ARObjectManager : MonoBehaviour
{
    private static ARObjectManager instance;
    public static ARObjectManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<ARObjectManager>();
            }
            return instance;
        }
    }

    public const float X_SIZE_BOUND = 2.5f;
    public const float Y_SIZE_BOUND = 2.5f;
    public const float Z_SIZE_BOUND = 2.5f;

    public GameObject OriginObject { get; set; }
    public Material OriginOrganMaterial { get; set; }
    public List<Vector3> ListOfInitialPositionOfChildren { get; set; }
    public GameObject CurrentObject { get; set; }
    public float FactorScaleInitial { get; set; }
    Bounds boundOriginObject;
    public static event Action onInitOrganSuccessfully;
    public static event Action<string> onChangeCurrentObject;
    public static event Action<GameObject> onLoadedObjectAtRuntime;

    public void ScaleObjectWithBound(GameObject objectInstance)
    {
        boundOriginObject = Helper.CalculateBounds(objectInstance);
        FactorScaleInitial = Mathf.Min(Mathf.Min(X_SIZE_BOUND / boundOriginObject.size.x, Y_SIZE_BOUND / boundOriginObject.size.y), Z_SIZE_BOUND / boundOriginObject.size.z);
        objectInstance.transform.localScale = objectInstance.transform.localScale * FactorScaleInitial;
    }

    public void SetCollider(GameObject objectInstance)
    {
        var transforms = objectInstance.GetComponentsInChildren<Transform>();
        if (transforms.Length <= 0)
        {
            return;
        }

        foreach (var item in transforms)
        {
            item.gameObject.AddComponent<MeshCollider>();
            item.tag = TagConfig.ORGAN;
        }
    }

    /// <summary>
    /// Assign organ model to gameobject
    /// </summary>
    public void InitGameObject(GameObject newGameObject)
    {
        OriginObject = newGameObject;
        OriginObject.name = StaticLesson.LessonTitle;
        ScaleObjectWithBound(OriginObject);
        SetCollider(OriginObject);
        XrayManager.Instance.DictionaryMaterialOriginal.Clear();
        XrayManager.Instance.GetOriginalMaterial(OriginObject);
        ChangeCurrentObject(OriginObject);
        onInitOrganSuccessfully?.Invoke();
    }

    public void ChangeCurrentObject(GameObject newGameObject)
    {
        CurrentObject = newGameObject;
        ListOfInitialPositionOfChildren = Helper.GetListOfInitialPositionOfChildren(CurrentObject);
        onChangeCurrentObject?.Invoke(CurrentObject.name);
    }

    /// <summary>
    /// Author: sonvdh
    /// Purpose: Load object at runtime
    /// </summary>
    public async Task LoadObjectAtRunTime(string objectURL)
    {
        int lastIndex = objectURL.LastIndexOf("/", StringComparison.Ordinal);
        string modelName = objectURL.Remove(0, lastIndex + 1);
        string modelPathInLocalSource = Application.persistentDataPath + "/" + modelName; // path to model fil
        if (!File.Exists(modelPathInLocalSource))
        {
            // If model was not downloaded (not exist in local), then download it
            await DownloadObjectFromLogicServer(objectURL, modelPathInLocalSource);
        }
        // Load file from local to unity application
        LoadObjectFromLocal(modelPathInLocalSource);
    }

     /// <summary>
    /// Author: sonvdh
    /// Purpose: Download model from server
    /// </summary>
    async Task DownloadObjectFromLogicServer(string objectURL, string modelPathInLocalSource)
    {
        try
        {
            Debug.Log("sonvdh Starting download model ...");
            WebClient client = new WebClient();
            await client.DownloadFileTaskAsync(new Uri(objectURL), modelPathInLocalSource);
        }
        catch (Exception exception)
        {
            StartCoroutine(Helper.DisplayNotification(exception.Message));
        }
    }

    /// <summary>
    /// Author: sonvdh
    /// Purpose: Load model file from local to unity application
    /// </summary>
    void LoadObjectFromLocal(string modelPathInLocalSource)
    {
        try
        {
            Debug.Log($"sonvdh Starting load object from local");
            AssetLoaderContext assetLoaderContext = AssetLoader.LoadModelFromFile(
                modelPathInLocalSource,
                x =>
                {
                    GameObject loadedObject = x.RootGameObject;
                    loadedObject.SetActive(false);
                },
                x =>
                {
                    GameObject loadedObjectWithMaterials = x.RootGameObject.transform.GetChild(0).gameObject;
                    onLoadedObjectAtRuntime?.Invoke(loadedObjectWithMaterials);
                    Destroy(x.RootGameObject);
                }
            );
        }
        catch (Exception exception)
        {
            StartCoroutine(Helper.DisplayNotification(exception.Message));
        }
    }
}

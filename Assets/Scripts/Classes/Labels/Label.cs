using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Label
{
    public string labelId;
    public string labelName;
    public Coordinate coordinates;
    public string labelIndex;
    public string level;
    public string parentId;
    public string videoLabel;
    public string audioLabel;
}

[Serializable]
public class Coordinate
{
    public float x;
    public float y;
    public float z;
}

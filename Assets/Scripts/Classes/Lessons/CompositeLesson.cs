using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CompositeLesson
{
    public List<Lesson> lessons;
    public int totalPage;
}

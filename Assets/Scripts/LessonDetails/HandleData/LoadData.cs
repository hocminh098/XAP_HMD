using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

namespace LessonDetails
{
    public class LoadData : MonoBehaviour
    {
        #region Singleton
        private static LoadData instance;
        public static LoadData Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<LoadData>();
                }
                return instance;
            }
        }
        #endregion Singleton

        public static event Action onLoadLessonDetail;

        #region Private Method
        public async Task<LessonDetail> HandleGetLessonDetail(string lessonId)
        {
            try
            {
                APIResponse<LessonDetail[]> lessonDetailResponse = await UnityHttpClient.CallAPI<APIResponse<LessonDetail[]>>(ApiConfig.GET_LESSON_DETAIL + lessonId, UnityWebRequest.kHttpVerbGET, null);

                if (lessonDetailResponse.code == ApiConfig.SUCCESS_RESPONSE_CODE)
                {
                    return lessonDetailResponse.data[0];
                }
                else
                {
                    throw new Exception(lessonDetailResponse.message);
                }
            }
            catch (Exception exception)
            {
                StartCoroutine(Helper.DisplayNotification(exception.Message));
                return null;
            }
        }
        #endregion Private Method
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ApiConfig
{
    public static string DOMAIN_SERVER = "https://api.xrcommunity.org/v1/xap/";    
    // Authenticate
    public static string POST_LOGIN = "user/login";
    public static string GET_USER_INFOR = "user/userInfo";
    public static string GET_LIST_ORGANS = "organs/getListOrgans";
    public static string GET_LIST_LESSON_BY_ORGAN = "organs/getListLessonByOrgan?organId=";
    public static string GET_LIST_ALL_LESSON = "organs/getListLessonByOrgan";
    public static string GET_LESSON_DETAIL = "lessons/getLessonDetail/";
    public static string POST_CREATE_MEETING_ROOM = "meetings/createRoom ";
    public static string POST_JOIN_MEETING_ROOM = "meetings/joinRoom ";
    public static string POST_LEAVE_MEETING_ROOM = "meetings/leaveRoom ";
    public static string JSON_CONTENT_TYPE_VALUE = "application/json";
    public static string CONTENT_TYPE_KEY = "Content-Type";
    public static string AUTHORIZATION_KEY = "Authorization";
    public static int SUCCESS_RESPONSE_CODE = 200;
    public static int BAD_REQUEST_RESPONSE_CODE = 400;
    public static int SERVER_ERROR_RESPONSE_CODE = 500;
}

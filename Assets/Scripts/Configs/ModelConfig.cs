using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ModelConfig
{
    public const string PATH_MODEL = "Model/";
    public static Vector3 CENTER_POSITION = new Vector3(0f, 1.8f, 0f);
    public static string playerInstantiateAreaTag = "PlayerInstantiateArea";
}

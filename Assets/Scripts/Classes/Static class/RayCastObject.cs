using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayCastObject
{
    public GameObject HoverObject { get; set; }
    public Vector3 RaycastPosition { get; set; }

    public RayCastObject(GameObject _hoverObject, Vector3 _raycastPosition)
    {
        HoverObject = _hoverObject;
        RaycastPosition = _raycastPosition;
    }
}

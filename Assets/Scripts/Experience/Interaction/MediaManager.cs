using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
// using Experience;
// using YoutubePlayer;

public class MediaManager : MonoBehaviour
{
    private static MediaManager instance;
    public static MediaManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<MediaManager>();
            }
            return instance;
        }
    }

    public const int MAX_NUMBER_LETTERS = 25;
    public GameObject contentMedia;
    public Text nameModel;

    public GameObject sidebarAudio;
    public GameObject nodata;

    public enum MediaType
    {
        VIDEO,
        AUDIO
    }

    void OnEnable()
    {
        ARObjectManager.onInitOrganSuccessfully += OnInitOrganSuccessfully;
    }

    void OnDisable()
    {
        ARObjectManager.onInitOrganSuccessfully -= OnInitOrganSuccessfully;
    }

    void OnInitOrganSuccessfully()
    {
        SetNameObjectInListMedia();
    }

    public void SetNameObjectInListMedia()
    {
        nameModel.text = ARObjectManager.Instance.OriginObject.name;
    }

    public void ListMediaOnPanel()
    {
        if (StaticLesson.Video != "")
        {
            nodata.SetActive(false);
            AssignValueToItemMedia(MediaType.VIDEO,
                                    StaticLesson.LessonTitle,
                                    StaticLesson.Video,
                                    StaticLesson.Audio);
        }

        if (StaticLesson.Audio != "")
        {
            nodata.SetActive(false);
            AssignValueToItemMedia(MediaType.AUDIO,
                                    StaticLesson.LessonTitle,
                                    StaticLesson.Video,
                                    StaticLesson.Audio);
        }
        // For sub-organ
        if (StaticLesson.ListLabel.Length > 0)
        {
            nodata.SetActive(false);
            for (int i = 0; i < StaticLesson.ListLabel.Length; i++)
            {
                if (StaticLesson.ListLabel[i].videoLabel != "")
                {
                    AssignValueToItemMedia(MediaType.VIDEO,
                                    StaticLesson.ListLabel[i].labelName,
                                    StaticLesson.ListLabel[i].videoLabel,
                                    StaticLesson.ListLabel[i].audioLabel);
                }

                if (StaticLesson.ListLabel[i].audioLabel != "")
                {
                    AssignValueToItemMedia(MediaType.AUDIO,
                                    StaticLesson.ListLabel[i].labelName,
                                    StaticLesson.ListLabel[i].videoLabel,
                                    StaticLesson.ListLabel[i].audioLabel);
                }
            }
        }
    }

    public void AssignValueToItemMedia(MediaType typeMedia, string nameMedia, string videoURL, string audioURL)
    {
        GameObject itemMedia = Instantiate(Resources.Load(PathResourceConfig.PATH_UI_ITEM_MEDIA) as GameObject);

        // Data
        MediaOrganItem dataItemVideo = itemMedia.GetComponent<MediaOrganItem>();
        dataItemVideo.SetValueMediaOrganItem(nameMedia, videoURL, audioURL);

        // UI
        itemMedia.transform.SetParent(contentMedia.transform, false);

        if (typeMedia == MediaType.VIDEO)
        {
            itemMedia.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>(PathResourceConfig.VIDEO_ICON);
            itemMedia.transform.GetChild(1).GetComponent<Text>().text = Helper.ShortString(StringConfig.videoPreText + nameMedia, MAX_NUMBER_LETTERS);
            itemMedia.transform.GetComponent<Button>().onClick.AddListener(delegate { VideoManager.Instance.ShowVideo(dataItemVideo, itemMedia); });
        }
        else if (typeMedia == MediaType.AUDIO)
        {
            itemMedia.transform.GetChild(1).GetComponent<Text>().text = Helper.ShortString(StringConfig.audioPreText + nameMedia, MAX_NUMBER_LETTERS);
            itemMedia.transform.GetComponent<Button>().onClick.AddListener(delegate { AudioManager.Instance.ShowAudio(dataItemVideo, itemMedia); });
        }
    }

    public void DeleteAllIconTickOnItemMedia()
    {
        GameObject[] objectsWithTagIconTick = GameObject.FindGameObjectsWithTag(TagConfig.ICON_TICK);
        foreach (GameObject subObject in objectsWithTagIconTick)
        {
            subObject.SetActive(false);
        }
    }
}

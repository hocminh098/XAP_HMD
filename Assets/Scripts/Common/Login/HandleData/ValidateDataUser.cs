using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using System.Net.Mail;

public class ValidateDataUser : MonoBehaviour
{
    #region Singleton
    private static ValidateDataUser instance;
    public static ValidateDataUser Instance 
    {
        get {
            if (instance == null) {
                instance = FindObjectOfType<ValidateDataUser>();
            }
            return instance;
        }
    } 
    #endregion Singleton
    Regex regexEmail = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
    Regex regexPassword = new Regex(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,20}$");

    public bool IsValidEmail(string email)
    {
        Match match = regexEmail.Match(email);
        if (match.Success)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool IsValidPassword(string password)
    {
        Match match = regexPassword.Match(password);
        if (match.Success)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
